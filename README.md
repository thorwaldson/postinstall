## download installer
  * `curl -LO archfi.sf.net/archfi`
  * if sf is down use
    * `curl -LO matmoul.github.io/archfi`
## execute Installer
  * `sh archfi`
    * follow standarts
    * choose keyboardlayout/language
    * dont install archdi
    * go back
    * reboot (remove usb)
* login as root
  * `pacman -S base-devel git`
  * edit /etc/sudoers
    * uncomment `%wheel ALL=(ALL) ALL`
    * save with `:wq!`
  * `sudo useradd -m -G wheel <username>`
  * `sudo passwd <username>`
  * `exit`
* login with new user
  * `git clone https://codeberg.org/thorwaldson/postinstall`
  * `cd postinstall`
  * edit packages and extras
  * `sh install.sh`
