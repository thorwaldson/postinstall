#!/bin/sh

install_packages_paru(){
  cd $postinstall
  paru -Syu --noconfirm --skipreview --needed $(cat $postinstall/packages)
  cd $postinstall
  paru -Syu --noconfirm --skipreview --needed $(cat $postinstall/extras)
}

install_packages_yay(){
  cd $postinstall
  yay -S --needed $(cat packages)
  cd $postinstall
  yay -S --needed $(cat extras)
}

install_paru(){
  cd ~/
  git clone https://aur.archlinux.org/paru.git
  cd paru
  makepkg -si
  paru --gendb
  cd $postinstall
}

install_yay(){
  cd /opt
  sudo git clone https://aur.archlinux.org/yay.git
  sudo chown -R $USER:users ./yay
  cd yay
  makepkg -si
  cd $postinstall
}

dotfiles(){
  cd ~/
  git clone https://gitlab.com/Thorwaldson/dotfiles
  sudo chmod +x ~/dotfiles/scripts/
  sh ~/dotfiles/scripts/_setup
}

enable_things(){
  sudo systemctl enable ly
  sudo systemctl enable bluetooth
  sudo systemctl enable haveged
  sudo systemctl enable --now ufw
  sudo ufw enable > /dev/null
  echo 'source ~/.profile' | sudo tee -a /etc/ly/xsetup.sh > /dev/null
}

mirrors(){
  sudo cp mirrors /etc/pacman.d/mirrorlist
}

postinstall=$(pwd)
mirrors
echo $postinstall
echo ""
echo ""
echo "In the next Step you need to uncomment '[multilib]' and the following line"
echo "press ENTER to continue"
read
sudo vim /etc/pacman.conf
sudo pacman -Syyuu --noconfirm --needed man-db
install_paru
install_packages_paru
dotfiles
enable_things
sudo reboot
